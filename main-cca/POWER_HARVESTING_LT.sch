EESchema Schematic File Version 4
LIBS:poc_cca_hms-cache
LIBS:CC1101-cache
EELAYER 26 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 3 3
Title ""
Date ""
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L Device:C_Small C12
U 1 1 5BE15114
P 5850 3900
F 0 "C12" H 5942 3946 50  0000 L CNN
F 1 "10uF" H 5942 3855 50  0000 L CNN
F 2 "" H 5850 3900 50  0001 C CNN
F 3 "~" H 5850 3900 50  0001 C CNN
	1    5850 3900
	1    0    0    -1  
$EndComp
$Comp
L Device:L_Small L2
U 1 1 5BE1524E
P 4150 3100
F 0 "L2" V 4335 3100 50  0000 C CNN
F 1 "10uH" V 4244 3100 50  0000 C CNN
F 2 "" H 4150 3100 50  0001 C CNN
F 3 "~" H 4150 3100 50  0001 C CNN
	1    4150 3100
	0    -1   -1   0   
$EndComp
NoConn ~ 3600 3900
Wire Wire Line
	4050 3100 3600 3100
Wire Wire Line
	3600 3100 3600 3300
Wire Wire Line
	4250 3100 4700 3100
Wire Wire Line
	4700 3100 4700 3300
$Comp
L Device:R_Small_US R20
U 1 1 5BE16868
P 3100 4350
F 0 "R20" H 3168 4396 50  0000 L CNN
F 1 "200kR" H 3168 4305 50  0000 L CNN
F 2 "" H 3100 4350 50  0001 C CNN
F 3 "~" H 3100 4350 50  0001 C CNN
	1    3100 4350
	1    0    0    -1  
$EndComp
$Comp
L Device:C_Small C11
U 1 1 5BE1697C
P 3500 4500
F 0 "C11" H 3592 4546 50  0000 L CNN
F 1 "1uF" H 3592 4455 50  0000 L CNN
F 2 "" H 3500 4500 50  0001 C CNN
F 3 "~" H 3500 4500 50  0001 C CNN
	1    3500 4500
	1    0    0    -1  
$EndComp
$Comp
L Device:R_Small_US R21
U 1 1 5BE16A6E
P 5450 3750
F 0 "R21" H 5300 3700 50  0000 C CNN
F 1 "1MR" H 5300 3800 50  0000 C CNN
F 2 "" H 5450 3750 50  0001 C CNN
F 3 "~" H 5450 3750 50  0001 C CNN
	1    5450 3750
	-1   0    0    1   
$EndComp
$Comp
L Device:R_Small_US R22
U 1 1 5BE16B07
P 5450 4050
F 0 "R22" H 5518 4096 50  0000 L CNN
F 1 "301kR" H 5518 4005 50  0000 L CNN
F 2 "" H 5450 4050 50  0001 C CNN
F 3 "~" H 5450 4050 50  0001 C CNN
	1    5450 4050
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR?
U 1 1 5BE17B9C
P 4150 4800
F 0 "#PWR?" H 4150 4550 50  0001 C CNN
F 1 "GND" H 4155 4627 50  0000 C CNN
F 2 "" H 4150 4800 50  0001 C CNN
F 3 "" H 4150 4800 50  0001 C CNN
	1    4150 4800
	1    0    0    -1  
$EndComp
Text HLabel 4850 3900 2    50   Output ~ 0
PG_CHRG
Wire Wire Line
	6750 3500 6750 3800
Wire Wire Line
	4700 3500 5450 3500
Wire Wire Line
	5450 3650 5450 3500
Wire Wire Line
	4850 3900 4700 3900
Wire Wire Line
	5850 3800 5850 3500
NoConn ~ 4700 4100
NoConn ~ 7200 4300
Wire Wire Line
	5450 3850 5450 3900
Connection ~ 5450 3500
Connection ~ 5850 3500
Wire Wire Line
	5450 3500 5850 3500
Wire Wire Line
	5850 3500 6200 3500
Wire Wire Line
	5450 3900 5300 3900
Wire Wire Line
	5300 3900 5300 3700
Wire Wire Line
	5300 3700 4700 3700
Connection ~ 5450 3900
Wire Wire Line
	5450 3900 5450 3950
Wire Wire Line
	6300 4700 6200 4700
Wire Wire Line
	6200 4700 6200 4100
Connection ~ 6200 3500
Wire Wire Line
	6200 3500 6750 3500
Wire Wire Line
	6300 4100 6200 4100
Connection ~ 6200 4100
Wire Wire Line
	6200 4100 6200 3500
NoConn ~ 6300 4500
NoConn ~ 6300 4300
$Comp
L power:GND #PWR?
U 1 1 5BE1B7BB
P 6750 5150
F 0 "#PWR?" H 6750 4900 50  0001 C CNN
F 1 "GND" H 6755 4977 50  0000 C CNN
F 2 "" H 6750 5150 50  0001 C CNN
F 3 "" H 6750 5150 50  0001 C CNN
	1    6750 5150
	1    0    0    -1  
$EndComp
Wire Wire Line
	6700 5050 6700 5100
Wire Wire Line
	6700 5100 6750 5100
Wire Wire Line
	6800 5100 6800 5050
Wire Wire Line
	6750 5100 6750 5150
Connection ~ 6750 5100
Wire Wire Line
	6750 5100 6800 5100
Wire Wire Line
	3500 4600 3500 4650
Wire Wire Line
	3500 4650 4100 4650
Wire Wire Line
	4150 4650 4150 4800
Wire Wire Line
	4100 4500 4100 4650
Connection ~ 4100 4650
Wire Wire Line
	4100 4650 4150 4650
Wire Wire Line
	4150 4650 4200 4650
Wire Wire Line
	4200 4650 4200 4500
Connection ~ 4150 4650
Wire Wire Line
	4200 4650 5000 4650
Wire Wire Line
	5450 4650 5450 4150
Connection ~ 4200 4650
Wire Wire Line
	5850 4000 5850 4650
Wire Wire Line
	5850 4650 5450 4650
Connection ~ 5450 4650
Wire Wire Line
	3500 4400 3500 4300
Wire Wire Line
	3500 4300 3600 4300
Wire Wire Line
	3600 4100 3100 4100
Wire Wire Line
	3100 4100 3100 4250
Wire Wire Line
	3100 4450 3100 4650
Wire Wire Line
	3100 4650 3500 4650
Connection ~ 3500 4650
Wire Wire Line
	2750 3700 2750 3100
Wire Wire Line
	2750 3100 3600 3100
Connection ~ 3600 3100
Wire Wire Line
	2750 3900 2750 4650
Wire Wire Line
	2750 4650 3100 4650
Connection ~ 3100 4650
Connection ~ 2750 3100
Connection ~ 2750 4650
$Comp
L Device:Battery_Cell BT2
U 1 1 5BE284D8
P 7650 4900
F 0 "BT2" H 7768 4996 50  0000 L CNN
F 1 "Battery_Cell" H 7768 4905 50  0000 L CNN
F 2 "" V 7650 4960 50  0001 C CNN
F 3 "~" V 7650 4960 50  0001 C CNN
	1    7650 4900
	1    0    0    -1  
$EndComp
Wire Wire Line
	7200 4500 7650 4500
Wire Wire Line
	7650 4500 7650 4700
Wire Wire Line
	7650 5000 7650 5100
Wire Wire Line
	7650 5100 6800 5100
Connection ~ 6800 5100
Wire Wire Line
	4700 4300 5000 4300
Wire Wire Line
	5000 4300 5000 4650
Connection ~ 5000 4650
Wire Wire Line
	5000 4650 5450 4650
$Comp
L Device:C_Small C10
U 1 1 5BE152FE
P 2750 3800
F 0 "C10" H 2842 3846 50  0000 L CNN
F 1 "10uF" H 2842 3755 50  0000 L CNN
F 2 "" H 2750 3800 50  0001 C CNN
F 3 "~" H 2750 3800 50  0001 C CNN
	1    2750 3800
	1    0    0    -1  
$EndComp
$Comp
L Connector:Conn_01x02_Male J4
U 1 1 5BE2DED5
P 1800 3750
F 0 "J4" H 1650 4000 50  0000 C CNN
F 1 "SOLAR_2" H 1650 3900 50  0000 C CNN
F 2 "" H 1800 3750 50  0001 C CNN
F 3 "~" H 1800 3750 50  0001 C CNN
	1    1800 3750
	1    0    0    -1  
$EndComp
Wire Wire Line
	2000 3750 2000 3100
Wire Wire Line
	2000 3100 2750 3100
Wire Wire Line
	2000 3850 2000 4650
Wire Wire Line
	2000 4650 2750 4650
$Comp
L capstone_library:LTC3105 U2
U 1 1 5BE24B21
P 4150 3800
AR Path="/5BE24B21" Ref="U2"  Part="1" 
AR Path="/5BE0DDE3/5BE24B21" Ref="U2"  Part="1" 
F 0 "U2" H 3350 4050 50  0000 C CNN
F 1 "LTC3105" H 3350 3950 50  0000 C CNN
F 2 "Package_SO:MSOP-12_3x4mm_P0.65mm" H 4150 2750 50  0001 C CNN
F 3 "https://www.analog.com/media/en/technical-documentation/data-sheets/3105fb.pdf" H 4150 2600 50  0001 C CNN
	1    4150 3800
	1    0    0    -1  
$EndComp
$Comp
L capstone_library:LTC4071 U3
U 1 1 5BE25083
P 6750 4650
F 0 "U3" H 6750 5678 50  0000 C CNN
F 1 "LTC4071" H 6750 5587 50  0000 C CNN
F 2 "Package_SO:MSOP-8-1EP_3x3mm_P0.65mm_EP2.54x2.8mm" H 6750 3750 50  0001 C CNN
F 3 "https://www.analog.com/media/en/technical-documentation/data-sheets/4071fc.pdf" H 6750 3600 50  0001 C CNN
	1    6750 4650
	1    0    0    -1  
$EndComp
$EndSCHEMATC
